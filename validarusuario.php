<?php

if(isset($_GET['i'])) {
    $codigo = $_GET['i'];
    $file = "json/$codigo.json";
    $return = "";
    if(file_exists($file)){
        $contents = file_get_contents($file);
        $return = json_decode($contents);
    }else{
        $template = "json/template.json";
        $newfile = "json/$codigo.json";
        if (!copy($template, $newfile)) {
            echo "Error al copiar $template...\n";
        }
        $return = 0;
    }
    echo json_encode($return);   
}