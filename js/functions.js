$(window).load(function(){
    $("#verificar").click(evt => {
        var codigo = $("#cod").val();
        if(codigo !=""){
            $.ajax({
                url: "validarusuario.php?i=" + codigo,
                method: "GET"
              }).done(function(data) {
                if(data == 0){
                  alert("El estudiante no esta registrado, se ha creado un nuevo archivo");
                }else{
                  $("#titulod").html('<h2 id="titulod">'+'Código del estudiante: '+codigo+'</h2>');
                  var htmlcontent = "";
                  data = JSON.parse(data);
                  htmlcontent += '<div class="din">';
                  for(i=0 ; i<=10 ; i++){
                    var band = false;
                    var htmlcontentaux = "";
                    for(j=0 ; j<data.materias.length ; j++){
                      if(i+1==data.materias[j].semestre){
                        band = true;                        
                        var nombre = data.materias[j].nombre;
                        var creditos = data.materias[j].creditos;
                        htmlcontentaux+= '<tr><td>'+nombre+'</td><td>'+creditos+'</td></tr>';
                      }
                    }
                    if(band){
                      htmlcontent += '<h3> Materia(s) del semestre '+(i+1)+':</h3>';
                      htmlcontent += '<table id = "tb">'+
                                          '<tr>'+
                                            '<td><strong>Nombre</strong></td>'+
                                            '<td><strong>Creditos</strong></td>'+
                                          '</tr>';
                      htmlcontent += htmlcontentaux+'</table>';
                      band = false;
                    }
                  }
                  htmlcontent += '<a href="index.html"><input id="volver" type="button" value="volver"></a>';
                  
                  htmlcontent += '</div>';
                  $(".dinamic").html(htmlcontent);
                }
              });
        }else{
          alert("debes ingresar un valor");
        }
      });
});

function  reloadData(){
  window.location('index.html');
  /*$("#titulod").html('<h2 id="titulod">Para ver los programas ofertados seleccione una facultad</h2>');
  $(".din").html('<input id="cod" type="text" name="codigo" >'+
                 '<input id="verificar" type="button" value="verificar">');*/
}